<?php

namespace App\Repository;

use App\Entity\Team;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
//use Doctrine\ORM\Query\AST\Join;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;


/**
 * @method Team|null find($id, $lockMode = null, $lockVersion = null)
 * @method Team|null findOneBy(array $criteria, array $orderBy = null)
 * @method Team[]    findAll()
 * @method Team[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TeamRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Team::class);
    }
    
    /**
     * Union of two different arrays
     * @param array $teams
     * @param array $sumExpValues
     * @return array $teams
     */
    public function mergeTeamListExp($teams,$sumExpValues){
        foreach($teams as $k=>$team){
            foreach($sumExpValues as $sumExpValue){
                if($sumExpValue['team_id'] == $team['team_id']){
                    $teams[$k]['exp_value'] = $sumExpValue['base_Exp_value_tot'];
                }
            }
        }
        return $teams;
    }
    
    /**
     * Filter bar and list query
     * @param obj $qb
     * @param string $type
     * @return array
     */
    public function findJoinByType($qb,$type=''){
        $qb->select('t.id as team_id','t.name', 'p.id as pkm_id', 'p.name as pk_name','p.ability','p.type','p.image_path')
            ->from('App\Entity\Team', 't')
            ->leftJoin(
                'App\Entity\Pokemon',
                'p',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                'p.team_id = t.id')
            ->orderBy('t.created_at', 'DESC');
        
        if($type!=''){
            $qb->where("p.type LIKE :type ")->setParameter('type', '%'.$type.'%');
        }

        return $qb->getQuery()
                ->useQueryCache(true)
                ->useResultCache(true,3600)
                ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }
    
    /**
     * Sum of base experiences
     * @param obj $qb
     * @return array
     */
    public function getSumExpTeam($qb) {
        $qb->select('t.id as team_id','sum(p.base_exp_value) as base_Exp_value_tot')
        ->from('App\Entity\Pokemon', 'p')
        ->leftJoin(
            'App\Entity\Team',
            't',
            \Doctrine\ORM\Query\Expr\Join::WITH,
            't.id = p.team_id')
        ->groupBy('p.team_id')
        ->orderBy('t.created_at', 'DESC');

        return $qb->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }
    
    /**
     * findOneByField
     * @param string $field
     * @param string $value
     * @return array
     */
    public function findOneByField($field, $value)
    {
        return $this->createQueryBuilder('t')
                ->select('t.id')
                ->andWhere('t.'.$field.' = :val')
                ->setParameter('val', $value)
                ->getQuery()
                ->getOneOrNullResult(\Doctrine\ORM\Query::HYDRATE_ARRAY)
        ;
    }
    
    
    public function delete($id){
         $conn = $this->getEntityManager()->getConnection();

        $sql = '
            delete FROM team
            WHERE id = :id
            ';
        $stmt = $conn->prepare($sql);
        $stmt->execute(['id' => $id]);
    }
    
       /**
     * countItems
     * @param int $team_id
     * @return int
     */
    public function countItems($name){
        $qb = $this->createQueryBuilder('t');
        $qb->select('count(t.id)')
            ->where('t.name LIKE :name')
            ->setParameter('name', $name);

        return $qb->getQuery()->getSingleScalarResult();
    }
}
