<?php

namespace App\Repository;

use App\Entity\Pokemon;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Pokemon|null find($id, $lockMode = null, $lockVersion = null)
 * @method Pokemon|null findOneBy(array $criteria, array $orderBy = null)
 * @method Pokemon[]    findAll()
 * @method Pokemon[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PokemonRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Pokemon::class);
    }
    
    
    /**
     * Saving image on the local filesystem
     * @param string $imageName
     */
    public function getAndSaveImage($imageName){
        $ch = curl_init('https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/'.$imageName);
        $fp = fopen('/var/www/public/images/'.$imageName, 'wb');
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_exec($ch);
        curl_close($ch);
        fclose($fp);
    }
    
    /**
     * Delete
     * @param int $id
     */
    public function delete($id){
         $conn = $this->getEntityManager()->getConnection();

        $sql = '
            delete FROM pokemon
            WHERE team_id = :id
            ';
        $stmt = $conn->prepare($sql);
        $stmt->execute(['id' => $id]);
    }
    
    /**
     * countItems
     * @param int $team_id
     * @return int
     */
    public function countItems($team_id){
        $qb = $this->createQueryBuilder('p');
        $qb->select('count(p.id)')
            ->where('p.team_id =  :team_id')
            ->setParameter('team_id', $team_id);

        return $qb->getQuery()->getSingleScalarResult();
   }

    public function countItemsFindByNameTeamid($name, $team_id){
        $qb = $this->createQueryBuilder('p');
        $qb->select('count(p.id)')
            ->where('p.team_id =  :team_id')
            ->setParameter('team_id', $team_id)
            ->andWhere('p.name LIKE :name')
            ->setParameter('name', $name);

        return $qb->getQuery()->getSingleScalarResult();
    }

}
