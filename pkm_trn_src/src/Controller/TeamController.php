<?php

namespace App\Controller;

use \App\Entity\Pokemon;
use \App\Entity\Team;
use \App\Form\TeamType;
use \App\Form\FilterTeamType;
use \App\Form\CreateTeamType;
use \App\Repository\TeamRepository;
use \App\Repository\PokemonRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/team")
 */
class TeamController extends AbstractController
{
    /**
     * @Route("/list", name="team_index", methods={"GET","POST"})
     */
    public function index(Request $request, TeamRepository $teamRepository): Response
    {
        $teams = array();
        $cacheDriver = new \Doctrine\Common\Cache\PhpFileCache(
            '/var/www/var/cache'
        );
        $config = new \Doctrine\ORM\Configuration();
        $config->setResultCacheImpl($cacheDriver);
        $pokemon = new Pokemon();
        $teamRep = $this->getDoctrine()->getRepository(Team::class);
        
        $form = $this->createForm(FilterTeamType::class, $pokemon);
        $form->handleRequest($request);
        
        $entityManager = $this->getDoctrine()->getManager();
        $qb = $entityManager->createQueryBuilder();

        if ($form->isSubmitted() && $form->isValid()) {//filter bar
            $data = $form->getData();
            $teams = $teamRep->findJoinByType($qb, $data->getType());
        }
        else{
            $teams = $teamRep->findJoinByType($qb);
            
        }

        if (!$teams) {
            return $this->render('team/index.html.twig', [
                    'teams' =>$teams,
                    'form' => $form->createView()
                ]);
        }
        else{
            $qb2 = $entityManager->createQueryBuilder();
            $sumExpValues = $teamRep->getSumExpTeam($qb2);
            $teams = $teamRep->mergeTeamListExp($teams,$sumExpValues);
            
            return $this->render('team/index.html.twig', [
                    'teams' =>$teams,
                    'form' => $form->createView()
                ]);
        }
    }

    /**
     * @Route("/create", name="team_new", methods={"GET","POST"})
     */
    public function newT(Request $request): Response
    {
        $team = new Team();
        $form = $this->createForm(CreateTeamType::class, $team);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $dataF = $form->getData();
            
            //all teams have a unique name. In the case of same name, cancel the action and go back to the list
            $teamRep = $this->getDoctrine()->getRepository(Team::class);
            $teamItems = $teamRep->countItems($dataF->getName());
            
            if($teamItems>0){
                return $this->redirectToRoute('team_index');
            }
            
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($team);
            $entityManager->flush();
            
            //flush cache
             $cacheDriver = new \Doctrine\Common\Cache\ArrayCache();
             $deleted = $cacheDriver->deleteAll();
    
            return $this->redirectToRoute('pokemon_new',['name'=>$dataF->getName()]);
        }

        return $this->render('team/new.html.twig', [
            'team' => $team,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="team_show", methods={"GET"})
     */
    public function show(Team $team): Response
    {
        return $this->render('team/show.html.twig', [
            'team' => $team,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="team_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Team $team): Response
    {
        $form = $this->createForm(TeamType::class, $team);
        
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $dataF = $form->getData();
            
            //all teams have a unique name. In the case of same name, cancel the action and go back to the list
            $teamRep = $this->getDoctrine()->getRepository(Team::class);
            $teamItems = $teamRep->countItems($dataF->getName());
            
            if($teamItems>0){
                return $this->redirectToRoute('team_index');
            }
            
            $this->getDoctrine()->getManager()->flush();
            
            //flush cache
            $cacheDriver = new \Doctrine\Common\Cache\ArrayCache();
             $deleted = $cacheDriver->deleteAll();
             
            return $this->redirectToRoute('team_index');
        }

        return $this->render('team/edit.html.twig', [
            'team' => $team,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="team_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Team $team): Response
    {
        if ($this->isCsrfTokenValid('delete'.$team->getId(), $request->request->get('_token'))) {
            // Deletion of all pokemons related to the team
            $pokemonRep = $this->getDoctrine()->getRepository(Pokemon::class);
            $pokemonRep->delete($team->getId());
            
            // Deletion of the team
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($team);
            $entityManager->flush();
        }

        return $this->redirectToRoute('team_index');
    }
}
