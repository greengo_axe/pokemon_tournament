<?php

namespace App\Controller;

use App\Entity\Pokemon;
use App\Entity\Team;
use App\Form\PokemonType;
use App\Form\CreatePokemonType;
use \App\Repository\PokemonRepository;
use \App\Repository\TeamRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/pokemon")
 */
class PokemonController extends AbstractController
{
    /**
     * @Route("/", name="pokemon_index", methods={"GET"})
     */
    public function index(PokemonRepository $pokemonRepository): Response
    {
        return $this->render('pokemon/index.html.twig', [
            'pokemon' => $pokemonRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new/{name}", name="pokemon_new", methods={"GET","POST"})
     */
    public function newP(Request $request,$name=''): Response
    {
        $rd='';
        $team_id = '';
        $pokemon = new Pokemon();
        $pokemonRep = $this->getDoctrine()->getRepository(Pokemon::class);
        $teamRep = $this->getDoctrine()->getRepository(Team::class);
        $form = $this->createForm(CreatePokemonType::class, $pokemon);
        $form->handleRequest($request);
        $rnd = rand(1,20);
        
        if ($form->isSubmitted() && $form->isValid()) {
            
            $teamValues= $teamRep->findOneByField('name',$name);//get team id
            $pokemon->setTeamId($teamValues['id']);
                        
            $pkmItems = $pokemonRep->countItemsFindByNameTeamid($pokemon->getName(),$pokemon->getTeamId());
		
	    if($pkmItems>0){
                return $this->redirectToRoute('pokemon_new',['name'=>$name]);
            }
            /*creation of a new pokemon*/
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($pokemon);
            $entityManager->flush();
            
            //save image on the local filesystem
            $dataF = $form->getData();
            $pokemonRep->getAndSaveImage($dataF->getImagePath());
            
            //flush cache
             $cacheDriver = new \Doctrine\Common\Cache\ArrayCache();
             $deleted = $cacheDriver->deleteAll();
             
            return $this->redirectToRoute('team_index');
        }

        return $this->render('pokemon/new.html.twig', [
            'pokemon' => $pokemon,
            'rnd' => $rnd,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="pokemon_show", methods={"GET"})
     */
    public function show(Pokemon $pokemon): Response
    {
        return $this->render('pokemon/show.html.twig', [
            'pokemon' => $pokemon,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="pokemon_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Pokemon $pokemon): Response
    {
        $form = $this->createForm(PokemonType::class, $pokemon);
        $form->handleRequest($request);
        $teamRep = $this->getDoctrine()->getRepository(Team::class);
        $pokemonRep = $this->getDoctrine()->getRepository(Pokemon::class);

        if ($form->isSubmitted() && $form->isValid()) {
            $pkmItems = $pokemonRep->countItemsFindByNameTeamid($pokemon->getName(),$pokemon->getTeamId());
		
	    if($pkmItems>0){
                return $this->redirectToRoute('pokemon_new',['name'=>$name]);
            }

            $this->getDoctrine()->getManager()->flush();
            
            //flush cache
             $cacheDriver = new \Doctrine\Common\Cache\ArrayCache();
             $deleted = $cacheDriver->deleteAll();
             
            return $this->redirectToRoute('team_index');
        }

        return $this->render('pokemon/edit.html.twig', [
            'pokemon' => $pokemon,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="pokemon_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Pokemon $pokemon): Response
    {
        
        if ($this->isCsrfTokenValid('delete'.$pokemon->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $pokemonRep = $this->getDoctrine()->getRepository(Pokemon::class);
            $teamRep = $this->getDoctrine()->getRepository(Team::class);
            $pkmItems = $pokemonRep->countItems($pokemon->getTeamId());
            
            if($pkmItems==1){//Deletion of team related
                $teamRep->delete($pokemon->getTeamId());
            }
            
            //pokemon deletion
            $entityManager->remove($pokemon);
            $entityManager->flush();
        }

        return $this->redirectToRoute('team_index');
    }
}
