# LNMP stack + Symfony 5 + Docker Demo (Pokemon Tournament)

### A simple dynamic web site, developed through Symfony 5, runs on LNMP stack(Linux/nginx/MySQL/PHP) dockerized

**You can get the Docker original boilerplate (Martin Pham's) **[here:](https://gitlab.com/martinpham/symfony-5-docker)

  * It is presumed that:
  > * Docker Compose is already installed on your PC. If not, [follow this guide:](https://docs.docker.com/compose/)
  >  * Docker Compose service is up and running
 
  * Open a console shell and run these commands:

>>$ git clone git@gitlab.com:greengo_axe/pokemon_tournament.git

>>$ cd pokemon_tournament

>>$ sudo chmod -R 777 pkm_trn_src (** you must grant all permissions to this folder (and its sub-folders)**)

>>$ cd docker

>>$ sudo docker-compose up

  * Open your favourite browser and insert this URL in the addresses bar : **localhost/team/list**
  * Enjoy!

  TODO:
  * To implement security
  * To add CSS style
